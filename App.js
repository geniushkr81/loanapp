import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from './screens/HomeScreen';
import RequestScreen from './screens/RequestScreen';
import ValidationScreen from './screens/ValidationScreen';
import PersonalScreen from './screens/PersonalScreen';
import ContactScreen from './screens/ContactScreen';
import JobScreen from './screens/JobScreen';
import ReferenceScreen from './screens/ReferenceScreen';
import BankScreen from './screens/BankScreen';
import FinalScreen from './screens/FinalScreen';
import SuccessScreen from './screens/SuccessScreen';
import {Provider} from 'react-redux';
import { createStore } from 'redux';
import LoanReducer from './LoanReducer';
import "react-datepicker/dist/react-datepicker.css";


const Stack = createNativeStackNavigator();
const store = createStore(LoanReducer);

const App = () => {
  return (
    <Provider store={store}>
    <NavigationContainer>
      <Stack.Navigator>
    
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{ headerShown:false}}
          />
          <Stack.Screen
          name="Request"
          component={RequestScreen}
          options={{ headerShown:false}}/>
          <Stack.Screen
          name="Validation"
          component={ValidationScreen}
          options={{ headerShown:false}}/>
          <Stack.Screen
          name="Personal"
          component={PersonalScreen}
          options={{ headerShown:false}}/>
          <Stack.Screen
          name="Contact"
          component={ContactScreen}
          options={{ headerShown:false}}/>
          <Stack.Screen
          name="Job"
          component={JobScreen}
          options={{ headerShown:false}}/>
          <Stack.Screen
          name="Reference"
          component={ReferenceScreen}
          options={{ headerShown:false}}/>
       <Stack.Screen
          name="Bank"
          component={BankScreen}
        options={{ headerShown:false}}/>
          <Stack.Screen 
          name="Final"
          component={FinalScreen}
          options={{ headerShown:false}}/>
          <Stack.Screen
          name="Success"
          component={SuccessScreen}
          options={{ headerShown:false}}/>
      </Stack.Navigator>
    </NavigationContainer>
    </Provider>
  )
}

export default App;