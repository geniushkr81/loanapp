import { combineReducers } from 'redux';

const INITIAL_STATE = {
    current: [],
    possible: [
        'Alice',
        'Bob',
        'Sammy',
    ],
};

const LoanReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'ADD_CLIENT':
            const {
                current, possible,
            } = state;

            const addedClient = possible.splice(action.payload, 1);

            current.push(addedClient);
            // finally , update the redux state
            const newState = {current, possible};
            return newState;
        default: return state
    }
};

export default combineReducers({
    friends: LoanReducer
});