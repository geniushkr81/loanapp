import React from 'react';
import { Button, StyleSheet, Text, TextInput, View, TouchableOpacity,Image, TouchableHighlight } from 'react-native';
import { Ionicons as Icon } from '@expo/vector-icons';
import Slider from '@react-native-community/slider';
import { Input } from 'react-native-elements';
import GradientButton from 'react-native-gradient-buttons';


export default class SuccessScreen extends React.Component {

    static defaultProps = {
        value:0,
    };
    state = {
        value: this.props.value,
    };

    // adding our home screen to the drawer navigation
    static navigationOptions = {
        drawerLabel: 'App prestamos',
    };

    // method to navigate to loan application screen
    apply(){
        const { navigate } = this.props.navigation
        navigate('Success')
    }
    // format currency
    currencyFormat(num) {
        return '$' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
     }
    // method to calculate amount and payments
    calculate(value){
        this.setState({value: value})
    }
    // method to navigate to loan history screen

    render() {
        return (
            <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity style={styles.innerHeader} onPress={() => {this.props.navigation.toggleDrawer();}}>
                  
                </TouchableOpacity>
            </View>
            <View style={styles.welcome}>
              <View style={{marginVertical: 10}}></View>
              <Text style={styles.welcomeMsg}>¡Hola, Nombre! Tu solicitud
de prestamo fue aprobada.</Text>
           
             
                <Text style={styles.blacktext}>Para concluir con esta solicitud y recibir tu préstamo en la cuenta bancaria que nos indicaste, es necesario pagar el monto de la comisión por apertura.
Una vez recibido tu pago, el monto del préstamo se acreditará en tu cuenta bancaria en un lapso de 12 a 24 horas.</Text>
<Text style={styles.blacktext}>$Monto de pago: $513.00</Text>
             
              <Text style={styles.codigo}>¡PrestaClick agradece tu preferencia!</Text>
              <View style={{ marginVertical: 20, backgroundColor:'gray' }}>
                <Text>Paga en OXXO
(Indica a la cajera que realizaras un pago con numero de referencia a OXXO PAY)

Tu numero de referencia:
9993-8985-5921-98</Text>  
              </View> 
            
                <View style={{ marginVertical: 20, backgroundColor:'gray' }}>
                  <Text>Paga via SPEI
(El siguiente numero de cuenta CLABE es unico, servira como referencia para validar el pago de tu comision por apertura, por favor paga el monto exacto)

Banco:
STP

Cuenta CLABE unica:
646010111844347709</Text>
                </View>
             


<hr></hr>
<Text style={styles.textpolitica}>Aviso:

Tu pago se procesara y validara automaticamente, no es necesario mandar un comprobante de este por ningun medio.

¡PrestaClick agradece tu preferencia!.</Text>

{/*}
              <TouchableHighlight onPress={() => this.viewLoans()}>
                <View style={styles.loanHistory}>
                      <Text style={{color: '#1f618d'}}>View Loan History</Text>
                </View>
        </TouchableHighlight> */}

            </View>
          </View>
        )
    }

}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: 0,
      backgroundColor: '#ffffff',
    },
    welcome: {
      alignItems: 'center',
      justifyContent: 'center',
      padding:20,
    },
    header: {
      backgroundColor: '#0175FF'
    },
    innerHeader: {
      padding: 20,
    },
    welcomeMsg: {
      padding: 20,
      fontSize: 20,
      textAlign: 'center',
      color: '#0175FF'
    },
    amount:{
        padding:5,
        fontSize:30,
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#000'
    },
    blacktext:{
        color:'#000',
        fontSize:20,
        fontWeight:'bold'
    },
    textpolitica:{
        color:'#000',
        fontSize:10,
        fontWeight:'normal'
    },
    codigo:{
        color:'gray',
        fontSize:14,
        textAlign:'left',
    },
    loanHistory: {
      borderColor: '#0175FF',
      borderWidth: 1,
      borderRadius: 4,
      padding: 20,
      alignItems: 'center',
      justifyContent: 'center',
    }
  });