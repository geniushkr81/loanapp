import React, {useState} from 'react';
import { Button, StyleSheet, Text, TextInput, View, TouchableOpacity,Image, TouchableHighlight } from 'react-native';
import { Ionicons as Icon } from '@expo/vector-icons';
import Slider from '@react-native-community/slider';
import { Input } from 'react-native-elements';
import GradientButton from 'react-native-gradient-buttons';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';

export default class BankScreen extends React.Component {

  constructor(props){
    super(props);
    this.bank = React.createRef();
    const getFunction = () => {
      AsyncStorage.getItem('Amount').then(value => 
        this.setState({amount:value})
      );
      AsyncStorage.getItem('Birdthday').then(value => 
        this.setState({birdthday:value})
      );
      AsyncStorage.getItem('Username').then(value => 
        this.setState({username:value})
      );
      AsyncStorage.getItem('FLastname').then(value => 
        this.setState({first_lastname:value})
      );
      AsyncStorage.getItem('SLastname').then(value => 
        this.setState({second_lastname:value})
      );
      AsyncStorage.getItem('Curp').then(value => 
        this.setState({curp:value})
      );
      AsyncStorage.getItem('Email').then(value => 
        this.setState({email:value})
      );
      AsyncStorage.getItem('Phone').then(value => 
        this.setState({phone:value})
      );
      AsyncStorage.getItem('Street').then(value => 
        this.setState({street:value})
      );
      AsyncStorage.getItem('CP').then(value => 
        this.setState({cp:value})
      );
      AsyncStorage.getItem('Colony').then(value => 
        this.setState({colony:value})
      );
      AsyncStorage.getItem('Country').then(value => 
        this.setState({country:value})
      );
      AsyncStorage.getItem('City').then(value => 
        this.setState({city:value})
      );
      AsyncStorage.getItem('Gov').then(value => 
        this.setState({government:value})
      );
      AsyncStorage.getItem('NRef01').then(value => 
        this.setState({nameref01:value})
      );
      AsyncStorage.getItem('NRef02').then(value => 
        this.setState({nameref02:value})
      );
      AsyncStorage.getItem('RelRef01').then(value => 
        this.setState({relref01:value})
      );
      AsyncStorage.getItem('RelRef02').then(value => 
        this.setState({relref02:value})
      );
      AsyncStorage.getItem('PhoneRef01').then(value => 
        this.setState({phoneref01:value})
      );
      AsyncStorage.getItem('PhoneRef02').then(value => 
        this.setState({phoneref02:value})
      );
      AsyncStorage.getItem('Occupation').then(value => 
        this.setState({occupation:value})
      );
      AsyncStorage.getItem('Company').then(value => 
        this.setState({company:value})
      );
      AsyncStorage.getItem('Address').then(value => 
        this.setState({address:value})
      );
      AsyncStorage.getItem('Salary').then(value => 
        this.setState({salary:value})
      );
      AsyncStorage.getItem('Holder').then(value => 
        this.setState({holder:value})
      );
      AsyncStorage.getItem('Clabe').then(value => 
        this.setState({clabe:value})
      );
      AsyncStorage.getItem('Bank').then(value => 
        this.setState({bank:value})
      );
    }
    getFunction();
  }

   // holder
   handleHolderChange = (e) => {
    this.state.holder = e.target.value;
   // alert(this.state.phone);
   AsyncStorage.setItem('Holder', this.state.holder);
  }
  // clabe
   handleClabeChange = (e) => {
    this.state.clabe = e.target.value;
    switch(e.target.value) {
      case '044':
        this.bank.current.value = "SCOTIABANK";
        break;
      case '166':
        this.bank.current.value = "BANSEFI";
        break;
        case '002':
          this.bank.current.value = "BANAMEX";
          break;    
          case '036':
            this.bank.current.value = "INBURSA";
            break;
            case '072':
              this.bank.current.value = "BANORTE";
              break;
              case '012':
              this.bank.current.value = "BANCOMER";
              break;       

              case '027':
              this.bank.current.value = "BANCO AZTECA";
              break;     

              case '014':
              this.bank.current.value = "SANTANDER";
              break;     

              case '021':
              this.bank.current.value = "HSBC";
              break;     
      default:
        //console.log('Bank not found!!!');
        this.bank.current.value="";
    }
   // alert(this.state.phone);
   AsyncStorage.setItem('Clabe', this.state.clabe);
  }
  // bank
  handleBankChange = (e) => {
    this.state.bank = e.target.value;
   // alert(this.state.phone);
   AsyncStorage.setItem('Bank', this.state.bank);
  }

    static defaultProps = {
        value:0,
    };
    state = {
        value: this.props.value,
        amount:'',
        holder:"",
        clabe:"",
        bank:"",
    };

    // adding our home screen to the drawer navigation
    static navigationOptions = {
        drawerLabel: 'App prestamos',
    };

    // method to navigate to loan application screen
    apply(){
      const client = {
        name:'',
        email: this.state.email,
        phone: this.state.phone,
        amount: this.state.amount,
        username: this.state.username,
        first_lastname:this.state.first_lastname,
        second_lastname:this.state.second_lastname,
        birdthday:this.state.birdthday,
        curp:this.state.curp,
        street:this.state.street,
        colony:this.state.colony,
        cp:this.state.cp,
        country:this.state.country,
        city:this.state.city,
        government:this.state.government,
        nameref01:this.state.nameref01,
        nameref02:this.state.nameref02,
        relref01:this.state.relref01,
        relref02:this.state.relref02,
        phoneref01:this.state.phoneref01,
        phoneref02:this.state.phoneref02,
        occupation: this.state.occupation,
        company: this.state.company,
        address: this.state.address,
        salary: this.state.salary,
        holder:this.state.holder,
        clabe:this.state.clabe,
        bank:this.state.bank,
      };
      var holderValid = false;
      var clabeValid = false;
      var bankValid = false;
      if(this.state.holder === ""){
        alert("Por favor, introduce el titular.");
        holderValid = false;
      }else {
        holderValid = true;
      }
      if(this.state.clabe === ""){
        alert("Por favor, introduce tu CLABE interbancaria");
        clabeValid = false;
      }else {
        clabeValid = true;
      }
      {/*
      if(this.state.bank === ""){
        alert("El cambo Banco es requerido!!!");
        bankValid = false;
      }else {
        bankValid = true;
      }*/}
      if(holderValid && clabeValid){
       //alert(JSON.stringify(this.state));
       //const data = JSON.stringify(this.state);
       axios.post('https://prestaclick.com.mx:3100/clients', client).then(function(response) {
         console.log(response.data);
        //  const { navigate } = this.props.navigation;
       //   navigate('Validation'); 
       
       }).catch(function(error){
        console.log(error);
       });
        const { navigate } = this.props.navigation
        navigate('Final')
      }
    }
    // format currency
    currencyFormat(num) {
        return '$' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
     }
    // method to calculate amount and payments
    calculate(value){
        this.setState({value: value})
    }
    // method to navigate to loan history screen

    render() {
        return (
            <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity style={styles.innerHeader} onPress={() => {this.props.navigation.toggleDrawer();}}>
                  
                </TouchableOpacity>
            </View>
            <View style={styles.welcome}>
              <View style={{marginVertical: 10}}></View>
              <Text style={styles.welcomeMsg}>Cuenta bancaria</Text>
              <Text style={styles.amount}>Estas solicitando: ${this.state.amount} mxn</Text>
             <p></p>
             <form>
              <Text style={styles.blacktext}>¿En donde depositamos tu préstamo?</Text>
             <Input placeholder='Titular' name="holder" onChange={e => this.handleHolderChange(e)} />
             <Input placeholder='Numero de cuenta CLABE' name="clabe"  onChange={e => this.handleClabeChange(e)}/>
             <input placeholder='Banco' name="bank" style={{ borderColor:'#bbbbbb', borderWidth:0, borderBottomWidth:1, height: 25, 
    width: 295, fontSize:18,padding:15, marginLeft:9 }}  ref={this.bank} readOnly="true"/>
             
             
              <View style={{ marginVertical: 20 }}></View>  <GradientButton
      style={{ marginVertical: 8 }}
      textStyle={{ fontSize: 20 }}
      gradientBegin="#0175FF"
      gradientEnd="#0016FF"
      gradientDirection="vertical"
      height={50}
      width={300}
      radius={25}
      impact
      impactStyle='Light'
      onPressAction={() => this.apply()}>
      Continuar
    </GradientButton></form>
                <View style={{ marginVertical: 20 }}></View>
<Text style={styles.blacktext}>Información legal</Text>

<Text style={styles.welcomeMsg}>Términos y condiciones</Text>

<Text style={styles.welcomeMsg}>Política y Aviso de Privacidad</Text>
<hr></hr>
<Text style={styles.textpolitica}>Tasa de interés mensual ordinaria aplicable desde 3.99% sin IVA. Por defecto, la tasa de interés anual fija es de 47.8% más IVA por un crédito de $70,000. Costo Anual Total (CAT) Promedio Ponderado: 106.75% sin IVA para fines informativos. Calculado a un plazo de 60 quincenas. Fecha de Cálculo: Febrero 28 2021.
Es el costo anual total del financiamiento expresado en términos de porcentaje anual, que para efectos informativos y comparativos, incluye todos los costos y gastos relacionados a tu préstamo. CAT máximo anual sin IVA: 61.7% por un crédito de $70,000 en 60 pagos quincenales. Tiempo mínimo de pago: 61 día después de emitida la línea de crédito. 
Tiempo máximo de pago: 900 días o 60 quincenas. TAE mínimo 60.65% - TAE máximo. Ejemplo: Crédito de 70.000 MXN a devolver en 30 meses. Interés fijo anual 47.88%. Cuota mensual 4.265.02 MXN. Sin comisión de apertura, estudio o por cancelación anticipada. TAE 74.74%. Cantidad total a devolver 127,950.60 MXN. El tipo de interés variará en función del importe.</Text>

{/*}
              <TouchableHighlight onPress={() => this.viewLoans()}>
                <View style={styles.loanHistory}>
                      <Text style={{color: '#1f618d'}}>View Loan History</Text>
                </View>
        </TouchableHighlight> */}
<br></br><Image source={require('../assets/logo-ssl-trust.png')} style={{flex:1,width:100, height:35}} />
            </View>
          </View>
        )
    }

}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: 0,
      backgroundColor: '#ffffff',
    },
    welcome: {
      alignItems: 'center',
      justifyContent: 'center',
      padding:20,
    },
    header: {
      backgroundColor: '#0175FF'
    },
    innerHeader: {
      padding: 20,
    },
    welcomeMsg: {
      padding: 20,
      fontSize: 20,
      textAlign: 'center',
      color: '#0175FF'
    },
    amount:{
        padding:5,
        fontSize:30,
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#000'
    },
    blacktext:{
        color:'#000',
        fontSize:20,
        fontWeight:'bold'
    },
    textpolitica:{
        color:'#000',
        fontSize:10,
        fontWeight:'normal'
    },
    codigo:{
        color:'gray',
        fontSize:14,
        textAlign:'left',
    },
    loanHistory: {
      borderColor: '#0175FF',
      borderWidth: 1,
      borderRadius: 4,
      padding: 20,
      alignItems: 'center',
      justifyContent: 'center',
    }
  });