import React, {useState} from 'react';
import { Button, StyleSheet, Text, TextInput, View, TouchableOpacity, Image,TouchableHighlight } from 'react-native';
import { Ionicons as Icon } from '@expo/vector-icons';
import Slider from '@react-native-community/slider';
import { Input } from 'react-native-elements';
import GradientButton from 'react-native-gradient-buttons';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import { Formik, Field } from 'formik';


export default class ContactScreen extends React.Component {
  constructor(props){
    super(props)
    this.colony = React.createRef();
    this.country = React.createRef();
    this.city = React.createRef();
    this.government = React.createRef();
    this.handleColonyChange = this.handleColonyChange.bind(this);
    // load data from Storage

    const getFunction = () => {
      AsyncStorage.getItem('Amount').then(value => 
        this.setState({amount:value})  
      );
      AsyncStorage.getItem('Birdthday').then(value => 
        this.setState({birdthday:value})
      );
      AsyncStorage.getItem('Username').then(value => 
        this.setState({username:value})
      );
      AsyncStorage.getItem('FLastName').then(value => 
        this.setState({first_lastname:value})
      );
      AsyncStorage.getItem('SLastName').then(value => 
        this.setState({second_lastname:value})
      );
      AsyncStorage.getItem('Curp').then(value => 
        this.setState({curp:value})
      );

    }
    getFunction();

    console.log(this.state.amount);
    console.log(this.state.birdthday);
    
  }

    static defaultProps = {
        value:0,
    };
    state = {
        value: this.props.value,
    };

    // street
    handleStreetChange = (e) => {
      this.state.street = e.target.value;
     // alert(this.state.phone);
     AsyncStorage.setItem('Street', this.state.street);
    }
    //colony
    handleColonyChange =  (e) => {
      this.state.colony = e.target.value;
    //this.setState({colony:e.target.value });
     // alert(this.state.phone);
     AsyncStorage.setItem('Colony', this.state.colony);
     
    }
    //cp
    handleCpChange = async (e) => {
      this.state.cp = e.target.value;
      //this.colony.current.value = "Initial value....";
      // query api 
    
      axios.get('https://prestaclick.com.mx:3100/cp?value='+e.target.value ).then((response)=>{
        console.log(response.data[0].Colonia);
       
       // this.state.colony = response.data[0].Colonia;
        this.colony.current.value = response.data[0].Colonia;
        this.state.colony = response.data[0].Colonia;
        this.country.current.value = response.data[0].Entidad;
        this.state.country = response.data[0].Entidad;
        this.city.current.value = response.data[0].Ciudad;
        this.state.city = response.data[0].Ciudad;
        this.government.current.value = response.data[0].Municipio;
        this.state.government = response.data[0].Municipio;
       // this.handleColonyChange(response.data[0].Colony);
      });
     // alert(this.state.phone);
     AsyncStorage.setItem('CP', this.state.cp);
    }
    //country
    handleCountryChange = (e) => {
      this.state.country = e.target.value;
     // alert(this.state.phone);
     AsyncStorage.setItem('Country', this.state.country);
    }
    //city
    handleCityChange = (e) => {
      this.state.city = e.target.value;
     // alert(this.state.phone);
     AsyncStorage.setItem('City', this.state.city);
    }
    //government
    handleGovChange = (e) => {
      this.state.government = e.target.value;
     // alert(this.state.phone);
     AsyncStorage.setItem('Gov', this.state.government);
    }

    static defaultProps = {
      value:0,
  };
  state = {
    text:'',
      value: this.props.value,
      phone: this.props.phone,
      email: this.props.email,
      amount: this.props.amount,
      username: this.props.username,
      first_lastname:this.props.first_lastname,
      second_lastname:this.props.second_lastname,
      birdthday:this.props.birdthday,
      curp:this.props.curp,
      street:'',
      colony:this.state.colony,
      cp:'',
      country:'',
      city:'',
      government:'',
  };

    // adding our home screen to the drawer navigation
    static navigationOptions = {
        drawerLabel: 'App prestamos',
    };

    // method to navigate to loan application screen
    handleSubmit(){
      var streetValid = false;
      var colonyValid = false;
      var cpValid = false;
      var countryValid = false;
      var cityValid = false;
      var govValid = false;
      if(this.state.street === ""){
        streetValid = false;
        alert("El campo calle es obligatorio!!!");
      }else {
        streetValid = true;
      }
      if(this.state.colony === ""){
        colonyValid = false;
        alert("El campo colonia es obligatorio!!!");
      }else {
        colonyValid = true;
      }
      if(this.state.cp === ""){
        cpValid = false;
        alert("El campo CP es obligatorio!!!");
      }else {
        cpValid = true;
      }
      if(this.state.country === ""){
        countryValid = false;
        alert("El campo estado es obligatorio!!!");
      }else {
        countryValid = true;
      }
      if(this.state.city === ""){
        cityValid = false;
        alert("El campo ciudad es obligatorio!!!");
      }else {
        cityValid = true;
      }
      if(this.state.government === ""){
        govValid = false;
        alert("El campo delegacion es obligatorio!!!");
      }else {
        govValid = true;
      }
      // Validate Email
        // const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        // validate only numbers
        const rx_live = /^[+-]?\d*(?:[.,]\d*)?$/;
      if(streetValid && colonyValid && cpValid && countryValid && cityValid && govValid){
        const { navigate } = this.props.navigation
        navigate('Job')
      }
       //console.log("Send form:" + e);
    }
    // format currency
    currencyFormat(num) {
        return '$' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
     }
    // method to calculate amount and payments
    calculate(value){
        this.setState({value: value})
    }
    // method to navigate to loan history screen

    render() {
        return (
            <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity style={styles.innerHeader} onPress={() => {this.props.navigation.toggleDrawer();}}>
                  
                </TouchableOpacity>
            </View>
            <View style={styles.welcome}>
              <View style={{marginVertical: 10}}></View>
              <Text style={styles.welcomeMsg}>Información de contacto</Text>
            {/*  <Text>Value:{this.state.email}</Text>*/} 
              <Text style={styles.amount}>Estas solicitando: ${this.state.amount} mxn</Text>
             <form binding={this}>
             <Input placeholder='Calle' name="street" onChange={e => this.handleStreetChange(e)}  />
             <Input placeholder='Código Postal' name="cp" onChange={async e => this.handleCpChange(e)}  />
             <input type="text" style={{ borderColor:'#bbbbbb', borderWidth:0, borderBottomWidth:1, height: 25, 
    width: 295, fontSize:18,padding:15,marginLeft:9 }} placeholder='Colonia' id="colony" ref={this.colony} name="colony"  />
             <input type="text" style={{ borderColor:'#bbbbbb', borderWidth:0, borderBottomWidth:1, height: 25, 
    width: 295, fontSize:18,padding:15, marginLeft:9 }}  placeholder='Delegación' name="country" ref={this.country}  />
             <input type="text" style={{ borderColor:'#bbbbbb', borderWidth:0, borderBottomWidth:1, height: 25, 
    width: 295, fontSize:18,padding:15, marginLeft:9 }}  placeholder='Ciudad' name="city" ref={this.city} />
             <input type="text" style={{ borderColor:'#bbbbbb', borderWidth:0, borderBottomWidth:1, height: 25, 
    width: 295, fontSize:18,padding:15, marginLeft:9 }}  placeholder='Estado' name="government" ref={this.government} />
             
              <View style={{ marginVertical: 20 }}></View>     <GradientButton
      style={{ marginVertical: 8 }}
      textStyle={{ fontSize: 20 }}
      gradientBegin="#0175FF"
      gradientEnd="#0016FF"
      gradientDirection="vertical"
      height={50}
      width={300}
      radius={25}
      impact
      impactStyle='Light'
      onPressAction={() => this.handleSubmit()}>
      Continuar
    </GradientButton>
    </form>
                <View style={{ marginVertical: 20 }}></View>
<Text style={styles.blacktext}>Información legal</Text>

<Text style={styles.welcomeMsg}>Términos y condiciones</Text>

<Text style={styles.welcomeMsg}>Política y Aviso de Privacidad</Text>
<hr></hr>
<Text style={styles.textpolitica}>Tasa de interés mensual ordinaria aplicable desde 3.99% sin IVA. Por defecto, la tasa de interés anual fija es de 47.8% más IVA por un crédito de $70,000. Costo Anual Total (CAT) Promedio Ponderado: 106.75% sin IVA para fines informativos. Calculado a un plazo de 60 quincenas. Fecha de Cálculo: Febrero 28 2021.
Es el costo anual total del financiamiento expresado en términos de porcentaje anual, que para efectos informativos y comparativos, incluye todos los costos y gastos relacionados a tu préstamo. CAT máximo anual sin IVA: 61.7% por un crédito de $70,000 en 60 pagos quincenales. Tiempo mínimo de pago: 61 día después de emitida la línea de crédito. 
Tiempo máximo de pago: 900 días o 60 quincenas. TAE mínimo 60.65% - TAE máximo. Ejemplo: Crédito de 70.000 MXN a devolver en 30 meses. Interés fijo anual 47.88%. Cuota mensual 4.265.02 MXN. Sin comisión de apertura, estudio o por cancelación anticipada. TAE 74.74%. Cantidad total a devolver 127,950.60 MXN. El tipo de interés variará en función del importe.</Text>

{/*}
              <TouchableHighlight onPress={() => this.viewLoans()}>
                <View style={styles.loanHistory}>
                      <Text style={{color: '#1f618d'}}>View Loan History</Text>
                </View>
        </TouchableHighlight> */}
<br></br><Image source={require('../assets/logo-ssl-trust.png')} style={{flex:1,width:100, height:35}} />
            </View>
          </View>
        )
    }

}
const styles = StyleSheet.create({
  textboxInput: { 
    height: 25, 
    width: 275, 
    borderWidth:0,
    borderBottomWidth:1,
    borderColor:'grey',
  } ,
    container: {
      flex: 1,
      marginTop: 0,
      backgroundColor: '#ffffff',
    },
    welcome: {
      alignItems: 'center',
      justifyContent: 'center',
      padding:20,
    },
    header: {
      backgroundColor: '#0175FF'
    },
    innerHeader: {
      padding: 20,
    },
    welcomeMsg: {
      padding: 20,
      fontSize: 20,
      textAlign: 'center',
      color: '#0175FF'
    },
    amount:{
        padding:5,
        marginBottom:5,
        fontSize:30,
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#000'
    },
    blacktext:{
        color:'#000',
        fontSize:20,
        fontWeight:'bold'
    },
    textpolitica:{
        color:'#000',
        fontSize:10,
        fontWeight:'normal'
    },
    codigo:{
        color:'gray',
        fontSize:14,
        textAlign:'left',
    },
    loanHistory: {
      borderColor: '#0175FF',
      borderWidth: 1,
      borderRadius: 4,
      padding: 20,
      alignItems: 'center',
      justifyContent: 'center',
    }
  });