import React from 'react';
import { Button, StyleSheet, Text, TextInput, View, TouchableOpacity, Image,TouchableHighlight } from 'react-native';
import { Ionicons as Icon } from '@expo/vector-icons';
import Slider from '@react-native-community/slider';
import { Input } from 'react-native-elements';
import GradientButton from 'react-native-gradient-buttons';
import * as Font from 'expo-font';
import  AsyncStorage  from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
import * as serviceWorker from '../serviceWorker';
import { createStore } from 'redux';
import logossl from '../assets/logo-ssl-trust.png';
import logobc from '../assets/logo-bc.png';


  const storeData = async (value) => {
      try {
        await AsyncStorage.setItem('@storage_key', value)
      } catch {
        // saving error
      }
  }

  const getData = async () => {
    try {
      const value = await AsyncStorage.getItem('@storage_key')
      if(value !== null){
        // value previously stored
        console.log(value);
      }
    } catch(e){
       // error reading value
    }
  }
class HomeScreen extends React.Component {



    static defaultProps = {
        value:5000,
    };
    state = {
        value: this.props.value,
     //   storeData: value,
    };

    // adding our home screen to the drawer navigation
    static navigationOptions = {
        drawerLabel: 'App prestamos',
    };

    // method to navigate to loan application screen
    apply(){
        const { navigate } = this.props.navigation
        navigate('Request',{ paramkey: this.state.value});
    }
    // format currency
    currencyFormat(num) {
        return '$' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
     }
    // method to calculate amount and payments
    calculate(value){
        this.setState({value: value});
       // getData();
    }
    // method to navigate to loan history screen

    render() {
        return (
            <View style={styles.container}>
          
            <View style={styles.header}>
                <TouchableOpacity style={styles.innerHeader} onPress={() => {this.props.navigation.toggleDrawer();}}>
         {/*         <Icon name="md-menu" size={30} style={{color: '#ffffff'}}/> */}
                </TouchableOpacity>
        </View> 
            <View style={styles.welcome}>
              <View style={{marginVertical: 10}}></View>
              <Text style={styles.welcomeMsg}>¿Cuánto dinero ocupas?</Text>
              <Text style={styles.amount}>${this.state.value && + this.state.value.toFixed(3)} mxn</Text>
             <View>
              <Slider 
                style={{width: 300, height: 40}}
                minimumValue={5000}
                value={5000}
                maximumValue={10000}
                thumbTintColor="#0175FF"
                minimumTrackTintColor="#001b33"
                maximumTrackTintColor="#000000"
                step={100}
                onValueChange={value => this.calculate(value)} />
                </View>
                {/* slider */}
                <Text style={styles.welcomeMsg}>Tu pago quincenal estimado:</Text>
                <Text style={styles.blacktext}>${((this.state.value / 14)*1.07).toFixed(2)} mxn</Text>
                <Text style={styles.blacktext}>Duración del préstamo: 14 quincenas</Text>
                <View style={{padding:20}}>
                <Text style={styles.codigo}>Codigo de invitacion</Text>
                <Input placeholder='codigo' />
                <Text style={styles.codigo}>Por el momento sólo puedes aprovechar los beneficios de PrestaClick teniendo un código de invitación.</Text>
                </View>
              <View style={{ marginVertical: 20 }}></View> 
             
              <GradientButton
      style={{ marginVertical: 8 }}
      textStyle={{ fontSize: 20 }}
      gradientBegin="#0175FF"
      gradientEnd="#0016FF"
      gradientDirection="vertical"
      height={50}
      width={300}
      radius={25}
      impact
      impactStyle='Light'
      onPressAction={() => this.apply()}
    >
      Iniciar Solicitud
    </GradientButton>

                <View style={{ marginVertical: 20 }}></View>
                <Text style={styles.blacktext}>Requisitos:</Text>
            <View style={{flexDirection:'column',flex:1,padding:20}}>
                <Text style={[styles.blacktext, {flex:1}]}>
Necesitarás: una identificación oficial (INE o PASAPORTE), 
una cuenta bancaria a tu nombre, tu comprobante de domicilio y un depósito de 
comisión por apertura equivalente al 5.49% + IVA del monto que solicitas.</Text>

                <View style={{ marginVertical: 20 }}></View>

<Text style={[styles.welcomeMsg,{fontSize:15, flex:1}]}>Términos y condiciones</Text>

<Text style={[styles.welcomeMsg,{fontSize:15}]}>Política y Aviso de Privacidad</Text>
<hr></hr>
<Text style={styles.textpolitica}>Tasa de interés mensual ordinaria aplicable desde 3.99% sin IVA. Por defecto, la tasa de interés anual fija es de 47.8% más IVA por un crédito de $70,000. Costo Anual Total (CAT) Promedio Ponderado: 106.75% sin IVA para fines informativos. Calculado a un plazo de 60 quincenas. Fecha de Cálculo: Febrero 28 2021.
Es el costo anual total del financiamiento expresado en términos de porcentaje anual, que para efectos informativos y comparativos, incluye todos los costos y gastos relacionados a tu préstamo. CAT máximo anual sin IVA: 61.7% por un crédito de $70,000 en 60 pagos quincenales. Tiempo mínimo de pago: 61 día después de emitida la línea de crédito. 
Tiempo máximo de pago: 900 días o 60 quincenas. TAE mínimo 60.65% - TAE máximo. Ejemplo: Crédito de 70.000 MXN a devolver en 30 meses. Interés fijo anual 47.88%. Cuota mensual 4.265.02 MXN. Sin comisión de apertura, estudio o por cancelación anticipada. TAE 74.74%. Cantidad total a devolver 127,950.60 MXN. El tipo de interés variará en función del importe.</Text>
<br></br><Image source={require('../assets/logo-ssl-trust.png')} style={{flex:1,width:100, height:35}} />

</View>
{/*}
              <TouchableHighlight onPress={() => this.viewLoans()}>
                <View style={styles.loanHistory}>
                      <Text style={{color: '#1f618d'}}>View Loan History</Text>
                </View>
        </TouchableHighlight> */}

            </View>
          </View>
        )
    }

}

export default HomeScreen;
const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: 0,
      flexDirection: 'column',
      backgroundColor: '#ffffff',
      fontFamily:'Montserrat-Regular'
    },
    welcome: {
        fontFamily:'Montserrat-Regular',
      alignItems: 'center',
      justifyContent: 'center',
    },
    header: {
      backgroundColor: '#0175FF'
    },
    innerHeader: {
      padding: 20,
    },
    buttonSend:{
        borderRadius:10,
        width:200,
        height:60,
        borderWidth: 1,
    },submit: {
        marginRight: 40,
        marginLeft: 40,
        marginTop: 10,
        paddingTop: 20,
        paddingBottom: 20,
        backgroundColor: ['#0175FF'],

//background: linear-gradient(180, "#0075FF", 28.13 , rgba(0, 194, 255, 0.75)),
        borderRadius: 25,
        borderWidth: 1,
        borderColor: '#fff',
        width:200,
      },
      submitText: {
        color: '#fff',
        fontSize:16,
        fontWeight:'bold',
        textAlign: 'center',
      }
    ,
    welcomeMsg: {
      padding: 10,
      fontSize: 24,
      textAlign: 'center',
      color: '#0175FF',
      fontWeight: 'bold'
    },
    amount:{
        padding:5,
        fontSize:40,
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#000'
    },
    blacktext:{
        color:'#000',
        fontSize:18,
        fontWeight:'bold'
    },
    textpolitica:{
        color:'#000',
        fontSize:10,
        fontWeight:'normal'
    },
    codigo:{
        color:'gray',
        fontSize:14,
        textAlign:'left',
    },
    loanHistory: {
      borderColor: '#1f618d',
      borderWidth: 1,
      borderRadius: 4,
      padding: 20,
      alignItems: 'center',
      justifyContent: 'center',
    }
  });