import React, {useState} from 'react';
import { Button, StyleSheet, Text,Picker, TextInput, View, TouchableOpacity,Image, TouchableHighlight } from 'react-native';
import { Ionicons as Icon } from '@expo/vector-icons';
import Slider from '@react-native-community/slider';
import { Input } from 'react-native-elements';
import GradientButton from 'react-native-gradient-buttons';
import AsyncStorage from '@react-native-community/async-storage';


export default class ReferenceScreen extends React.Component {

  constructor(props){
    super(props);
    const getPhoneFunction = () => {
      AsyncStorage.getItem('Amount').then(value => 
        this.setState({amount:value})
      );
    }
    getPhoneFunction();
  }

    static defaultProps = {
        value:0,
    };
    state = {
        value: this.props.value,
        amount:'',
        nameref01:"",
        nameref02:"",
        relref01:"",
        relref02:"",
        phoneref01:"",
        phoneref02:"",
    };
    // name references
    handleNameRef01Change = (e) => {
      this.state.nameref01 = e.target.value;
     // alert(this.state.phone);
     AsyncStorage.setItem('NRef01', this.state.nameref01);
    }
    handleNameRef02Change = (e) => {
      this.state.nameref02 = e.target.value;
     // alert(this.state.phone);
     AsyncStorage.setItem('NRef02', this.state.nameref02);
    }
    // relationship
    handleRelRef01Change = (e) => {
      this.state.relref01 = e.target.value;
     // alert(this.state.phone);
     AsyncStorage.setItem('RelRef01', this.state.relref01);
    }
    handleRelRef02Change = (e) => {
      this.state.relref02 = e.target.value;
     // alert(this.state.phone);
     AsyncStorage.setItem('RelRef02', this.state.relref02);
    }
    // phone references

    handlePhoneRef01Change = (e) => {
      this.state.phoneref01 = e.target.value;
     // alert(this.state.phone);
     AsyncStorage.setItem('PhoneRef01', this.state.phoneref01);
    }
    handlePhoneRef02Change = (e) => {
      this.state.phoneref02 = e.target.value;
     // alert(this.state.phone);
     AsyncStorage.setItem('PhoneRef02', this.state.phoneref02);
    }

    // adding our home screen to the drawer navigation
    static navigationOptions = {
        drawerLabel: 'App prestamos',
    };

    // method to navigate to loan application screen
    apply(){
      var nameref01Valid = false;
      var nameref02Valid = false;
      var relref01Valid = false;
      var relref02Valid = false;
      var phoneref01Valid = false;
      var phoneref02Valid = false;

      if(this.state.nameref01 === ""){ 
        alert("El nombre de tu primera referencia es requerido");
        nameref01Valid = false; }else{nameref01Valid = true;}
      if(this.state.nameref02 === ""){ 
        alert("El nombre de tu segunda referencia es requerido");
        nameref02Valid = false; }else{nameref02Valid = true;}
      if(this.state.relref01 === ""){ 
        alert("El campo relación con la referencia es requerido");
        relref01Valid = false; }else{relref01Valid = true;}
      if(this.state.relref02 === ""){ 
        alert("El campo relación con  la referencia es requerido");
        relref02Valid = false; }else{relref02Valid = true;}
      if(this.state.phoneref01 === ""){ 
        alert("El campo numero de referencia es requerido");
        phoneref01Valid = false; }else{phoneref01Valid = true;}
      if(this.state.phoneref02 === ""){
        alert("El campo numero de referencia es requerido");
        phoneref02Valid = false; }else{phoneref02Valid = true;}

      if(nameref01Valid && nameref02Valid && relref01Valid && relref02Valid && phoneref01Valid && phoneref02Valid){
        const { navigate } = this.props.navigation
        navigate('Bank')
      }
    }
    // format currency
    currencyFormat(num) {
        return '$' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
     }
    // method to calculate amount and payments
    calculate(value){
        this.setState({value: value})
    }
    // method to navigate to loan history screen

    render() {
        return (
            <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity style={styles.innerHeader} onPress={() => {this.props.navigation.toggleDrawer();}}>
                </TouchableOpacity>
            </View>
            <View style={styles.welcome}>
              <View style={{marginVertical: 10}}></View>
              <Text style={styles.welcomeMsg}>Informacion de contacto</Text>
              <Text style={styles.amount}>Estas solicitando: ${this.state.amount} mxn</Text>
             <form>
            <Text style={styles.blacktext}>Primera referencia</Text>
             <Input placeholder='Nombre(s)' name="name_ref01" onChange={e => this.handleNameRef01Change(e)}/>
             <Picker style={styles.picker} onChange={e => this.handleRelRef01Change(e)}>
             <Picker.Item label="--Seleccione--"/>
               <Picker.Item label="Amigo" value="Amigo"/>
               <Picker.Item label="Familiar" value="Familiar"/>
               <Picker.Item label="Compañero laboral" value="Compañero laboral"/>
             </Picker>
             <Input placeholder='Numero de contacto' name="phone_ref01" onChange={e => this.handlePhoneRef01Change(e)} />
             <View style={{ marginVertical: 20 }}></View>
             <Text style={styles.blacktext}>Segunda referencia</Text>
             <Input placeholder='Nombre(s)' name="name_ref02" onChange={e => this.handleNameRef02Change(e)} />
             {/*<Input placeholder='Parentesco' name="relation_ref02" onChange={e => this.handleRelRef02Change(e)}/> */}
             <Picker style={styles.picker} onChange={e => this.handleRelRef02Change(e)}>
             <Picker.Item label="--Seleccione--"/>
               <Picker.Item label="Amigo" value="Amigo"/>
               <Picker.Item label="Familiar" value="Familiar"/>
               <Picker.Item label="Compañero laboral" value="Compañero laboral"/>
             </Picker>
             <Input placeholder='Numero de contacto' name="phone_ref02" onChange={e => this.handlePhoneRef02Change(e)} />
             
              <View style={{ marginVertical: 20 }}></View>
              <GradientButton
      style={{ marginVertical: 8 }}
      textStyle={{ fontSize: 20 }}
      gradientBegin="#0175FF"
      gradientEnd="#0016FF"
      gradientDirection="vertical"
      height={50}
      width={300}
      radius={25}
      impact
      impactStyle='Light'
      onPressAction={() => this.apply()}
    >
      Continuar
    </GradientButton>
    </form>
                <View style={{ marginVertical: 20 }}></View>
<Text style={styles.blacktext}>Información legal</Text>

<Text style={styles.welcomeMsg}>Términos y condiciones</Text>

<Text style={styles.welcomeMsg}>Política y Aviso de Privacidad</Text>
<hr></hr>
<Text style={styles.textpolitica}>Tasa de interés mensual ordinaria aplicable desde 3.99% sin IVA. Por defecto, la tasa de interés anual fija es de 47.8% más IVA por un crédito de $70,000. Costo Anual Total (CAT) Promedio Ponderado: 106.75% sin IVA para fines informativos. Calculado a un plazo de 60 quincenas. Fecha de Cálculo: Febrero 28 2021.
Es el costo anual total del financiamiento expresado en términos de porcentaje anual, que para efectos informativos y comparativos, incluye todos los costos y gastos relacionados a tu préstamo. CAT máximo anual sin IVA: 61.7% por un crédito de $70,000 en 60 pagos quincenales. Tiempo mínimo de pago: 61 día después de emitida la línea de crédito. 
Tiempo máximo de pago: 900 días o 60 quincenas. TAE mínimo 60.65% - TAE máximo. Ejemplo: Crédito de 70.000 MXN a devolver en 30 meses. Interés fijo anual 47.88%. Cuota mensual 4.265.02 MXN. Sin comisión de apertura, estudio o por cancelación anticipada. TAE 74.74%. Cantidad total a devolver 127,950.60 MXN. El tipo de interés variará en función del importe.</Text>

{/*}
              <TouchableHighlight onPress={() => this.viewLoans()}>
                <View style={styles.loanHistory}>
                      <Text style={{color: '#1f618d'}}>View Loan History</Text>
                </View>
        </TouchableHighlight> */}
<br></br><Image source={require('../assets/logo-ssl-trust.png')} style={{flex:1,width:100, height:35}} />
            </View>
          </View>
        )
    }

}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: 0,
      backgroundColor: '#ffffff',
    },
    picker: {
      width:"90%",
      height:30,
      marginLeft:10,
      borderBottomWidth:1,
      borderWidth:0,
      borderBottomColor:"#6e6e6e",
    },
    welcome: {
      alignItems: 'center',
      justifyContent: 'center',
      padding:20,
    },
    header: {
      backgroundColor: '#0175FF'
    },
    innerHeader: {
      padding: 20,
    },
    welcomeMsg: {
      padding: 20,
      fontSize: 20,
      textAlign: 'center',
      color: '#0175FF'
    },
    amount:{
        padding:5,
        fontSize:30,
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#000'
    },
    blacktext:{
        color:'#000',
        fontSize:20,
        fontWeight:'bold'
    },
    textpolitica:{
        color:'#000',
        fontSize:10,
        fontWeight:'normal'
    },
    codigo:{
        color:'gray',
        fontSize:14,
        textAlign:'left',
    },
    loanHistory: {
      borderColor: '#0175FF',
      borderWidth: 1,
      borderRadius: 4,
      padding: 20,
      alignItems: 'center',
      justifyContent: 'center',
    }
  });