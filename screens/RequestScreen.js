import React, {useState} from 'react';
import { Button, StyleSheet, Text, TextInput, View, TouchableOpacity,Alert, Image,TouchableHighlight } from 'react-native';
import { Ionicons as Icon } from '@expo/vector-icons';
import Slider from '@react-native-community/slider';
import { Input } from 'react-native-elements';
import GradientButton from 'react-native-gradient-buttons';
import { SafeAreaInsetsContext } from 'react-native-safe-area-context';
import * as SMS from 'expo-sms';
import Expo from 'expo';
import axios from 'axios';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';



export default class RequestScreen extends React.Component {

 constructor(props){
   super(props);
  this.state = {
    name:'',
    email:'',
    phone:0,
    amount: '',
    error_email: '',
  }
  this.handleChange = this.handleChange.bind(this)
 }

 handleChange = (e) =>{

   this.state.email = e.target.value;
  // alert(this.state.email);
//   this.setState({[e.target.email]: e.target.value});
  AsyncStorage.setItem('Email', this.state.email);
  AsyncStorage.setItem('Amount', this.props.route.params.paramkey);
 }
 handleEmail = (e) => {
   this.state.remail = e.target.value;
 }
 // handle phone number
 handlePhone = (e) => {
   this.state.phone = e.target.value;
  // alert(this.state.phone);
  AsyncStorage.setItem('Phone', this.state.phone);
 }

 handleSubmit = (e) => {
  const client = {
    name:'',
    email: this.state.email,
    remail: this.state.remail,
    phone: this.state.phone,
    amount: this.state.amount
  };
   //alert(JSON.stringify(this.state));
   //const data = JSON.stringify(this.state);
   //axios.post('http://localhost:3100/clients', client).then(function(response) {
    //  console.log(response.data);
    //  const { navigate } = this.props.navigation;
   //   navigate('Validation');
  // }).catch(function(error){
   // console.log(error);
   //});
   var emailValid = false;
   var phoneValid = false;
   const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
   const rx_live = /^[+-]?\d*(?:[.,]\d*)?$/;

   if(reg.test(this.state.email) === false){
     alert("El campo email es requerido o el formato es incorrecto");
   }else{
     emailValid = true
   }
   if(reg.test(this.state.remail) === false){
    alert("El campo email es requerido");
   }else{
     emailValid = true
   }
   var cell_phone = parseInt(this.state.phone);
   if(rx_live.test(cell_phone)){     
      phoneValid = true
   }else{
     alert("El campo de celular debe ser numérico");
      phoneValid = false
   }
   if(emailValid && phoneValid) {
      const { navigate } = this.props.navigation;
      navigate('Personal');
   }
  //  e.preventDefault();
 }

  static defaultProps = {
        value: 0
    };
    state = {
        value: this.props.value,
    //    name: '',
     //   phone: '',
    //    email: '',
    };

    

    // adding our home screen to the drawer navigation
    static navigationOptions = {
        drawerLabel: 'App prestamos',
    };

    // method to navigate to loan application screen
    apply = () => {
      this.handleSubmit(e);
        const { navigate } = this.props.navigation;
        navigate('Validation');
    }
    // format currency
    currencyFormat(num) {
        return '$' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
     }
    // method to calculate amount and payments
    calculate(value){
        this.setState({value: value})
    }
    // method to navigate to loan history screen

    render() {
      
        return (
            <View style={styles.container}>
          
            <View style={styles.header}>
                <TouchableOpacity style={styles.innerHeader} onPress={() => {this.props.navigation.toggleDrawer();}}>
               {/*   <Icon name="md-menu" size={30} style={{color: '#ffffff'}}/> */}
                </TouchableOpacity>
            </View>
            <View style={styles.welcome}>
              <View style={{marginVertical: 10}}></View>
              <Text style={styles.welcomeMsg}>¡Tu código de invitación es correcto!</Text>
              <Text style={styles.amount}>Estas solicitando: ${this.props.route.params.paramkey} mxn</Text>
            <p></p>
            <form onSubmit={this.handleSubmit}>
             <Input type="email" name="email" id="email" label="E-mail" placeholder='E-mail' onChange={e => this.handleChange(e)} />
           
             <Input type="email" name="remail" id="remail" placeholder='Repite tu e-mail' onChange={e => this.handleEmail(e)}/>
             <Input type="number" id="phone" name="phone" pattern="[+-]?\d+(?:[.,]\d+)?" label="Teléfono de contacto" placeholder='Telefono movil de contacto' onChange={e => this.handlePhone(e)} />
             
             {/* <Text style={styles.codigo}>Recibiras un codigo de confirmacion por SMS con el fin de validar tu identidad.</Text> */}
              <View style={{ marginVertical: 20 }}></View>    <GradientButton
      style={{ marginVertical: 8 }}
      textStyle={{ fontSize: 20 }}
      gradientBegin="#0175FF"
      gradientEnd="#0016FF"
      gradientDirection="vertical"
      height={50}
      width={300}
      radius={25}
      impact
      impactStyle='Light'
      onPressAction={this.handleSubmit}
    >
      Continuar
    </GradientButton>
   
    </form>
                <View style={{ marginVertical: 20 }}></View>
<Text style={styles.blacktext}>Información legal</Text>

<Text style={styles.welcomeMsg}>Términos y condiciones</Text>

<Text style={styles.welcomeMsg}>Política y Aviso de Privacidad</Text>
<hr></hr>
<Text style={styles.textpolitica}>Tasa de interés mensual ordinaria aplicable desde 3.99% sin IVA. Por defecto, la tasa de interés anual fija es de 47.8% más IVA por un crédito de $70,000. Costo Anual Total (CAT) Promedio Ponderado: 106.75% sin IVA para fines informativos. Calculado a un plazo de 60 quincenas. Fecha de Cálculo: Febrero 28 2021.
Es el costo anual total del financiamiento expresado en términos de porcentaje anual, que para efectos informativos y comparativos, incluye todos los costos y gastos relacionados a tu préstamo. CAT máximo anual sin IVA: 61.7% por un crédito de $70,000 en 60 pagos quincenales. Tiempo mínimo de pago: 61 día después de emitida la línea de crédito. 
Tiempo máximo de pago: 900 días o 60 quincenas. TAE mínimo 60.65% - TAE máximo. Ejemplo: Crédito de 70.000 MXN a devolver en 30 meses. Interés fijo anual 47.88%. Cuota mensual 4.265.02 MXN. Sin comisión de apertura, estudio o por cancelación anticipada. TAE 74.74%. Cantidad total a devolver 127,950.60 MXN. El tipo de interés variará en función del importe.</Text>

{/*}
              <TouchableHighlight onPress={() => this.viewLoans()}>
                <View style={styles.loanHistory}>
                      <Text style={{color: '#1f618d'}}>View Loan History</Text>
                </View>
        </TouchableHighlight> */}
<br></br><Image source={require('../assets/logo-ssl-trust.png')} style={{flex:1,width:100, height:35}} />
            </View>
          </View>
        )
    }

}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: 0,
      
      backgroundColor: '#ffffff',
    },
    welcome: {
      alignItems: 'center',
      justifyContent: 'center',
      padding:20,
    },
    header: {
      backgroundColor: '#0175FF'
    },
    innerHeader: {
      padding: 20,
    },
    welcomeMsg: {
      padding: 10,
      fontSize: 20,
      textAlign: 'center',
      color: '#0175FF'
    },
    amount:{
        padding:5,
        fontSize:30,
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#000'
    },
    blacktext:{
        color:'#000',
        fontSize:20,
        fontWeight:'bold'
    },
    textpolitica:{
        color:'#000',
        fontSize:10,
        fontWeight:'normal'
    },
    codigo:{
        color:'gray',
        fontSize:14,
        textAlign:'left',
    },
    loanHistory: {
      borderColor: '#1f618d',
      borderWidth: 1,
      borderRadius: 4,
      padding: 20,
      alignItems: 'center',
      justifyContent: 'center',
    }
  });