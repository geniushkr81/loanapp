import React from 'react';
import { Button, StyleSheet, Picker, Text, TextInput, View, TouchableOpacity, Image,TouchableHighlight } from 'react-native';
import { Ionicons as Icon } from '@expo/vector-icons';
import Slider from '@react-native-community/slider';
import { Input } from 'react-native-elements';
import GradientButton from 'react-native-gradient-buttons';
import AsyncStorage from '@react-native-community/async-storage';

export default class JobScreen extends React.Component {
  constructor(props) {
    super(props)
    const getFunction = () => {
      AsyncStorage.getItem('Amount').then(value => 
        this.setState({amount:value})  
      );
    }
    getFunction();
  };

    static defaultProps = {
        value:0,
    };
    state = {
        value: this.props.value,
        amount: this.props.amount,
        occupation: "",
        company: "",
        address: "",
        salary: "",
    };
// occupation
    handleOccupationChange = (e) => {
      this.state.occupation = e.target.value;
     // alert(this.state.phone);
     AsyncStorage.setItem('Occupation', this.state.occupation);
    }
  // Company
  handleCompanyChange = (e) => {
    this.state.company = e.target.value;
   // alert(this.state.phone);
   AsyncStorage.setItem('Company', this.state.company);
  }
  // Address
  handleAddressChange = (e) => {
    this.state.address = e.target.value;
   // alert(this.state.phone);
   AsyncStorage.setItem('Address', this.state.address);
  }
  // Salary
  handleSalaryChange = (e) => {
    this.state.salary = e.target.value;
   // alert(this.state.phone);
   AsyncStorage.setItem('Salary', this.state.salary);
  }

    // adding our home screen to the drawer navigation
    static navigationOptions = {
        drawerLabel: 'App prestamos',
    };

    // method to navigate to loan application screen
    apply(){
      var occuValid = false;
      var companyValid = false;
      var addressValid = false;
      var salaryValid = false;
      if(this.state.occupation === ""){
        alert("Ingresa tu ocupación laboral");
        occuValid = false;
      }else{
        occuValid = true;
      }
      if(this.state.company === ""){
        alert("El nombre de empresa es obligatorio");
        companyValid = false;
      }else{
        companyValid = true;
      }
      if(this.state.address === ""){
        alert("Ingresa tu dirección laboral");
        addressValid = false;
      }else{
        addressValid = true;
      }
      if(this.state.salary === ""){
        alert("Selecciona tu rango de salario");
        salaryValid = false;
      }else{
        salaryValid = true;
      }
      if(occuValid && companyValid && addressValid && salaryValid){
        const { navigate } = this.props.navigation
        navigate('Reference')
      }
    }
    // format currency
    currencyFormat(num) {
        return '$' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
     }
    // method to calculate amount and payments
    calculate(value){
        this.setState({value: value})
    }
    // method to navigate to loan history screen

    render() {
        return (
            <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity style={styles.innerHeader} onPress={() => {this.props.navigation.toggleDrawer();}}>
                  
                </TouchableOpacity>
            </View>
            <View style={styles.welcome}>
              <View style={{marginVertical: 10}}></View>
              <Text style={styles.welcomeMsg}>Datos laborales</Text>
              <Text style={styles.amount}>Estas solicitando: ${this.state.amount} mxn</Text>
             <p></p>
             <form>
             {/*<Input placeholder='Profesion' name="occupation" onChange={e => this.handleOccupationChange(e)} />*/}
             <Picker style={styles.picker} onChange={e => this.handleOccupationChange(e)}>
             <Picker.Item label="--Seleccione--"/>
               <Picker.Item label="Empleado" value="Empleado"/>
               <Picker.Item label="Independiente" value="independiente"/>
               <Picker.Item label="Dueño de negocio" value="Dueño"/>
             </Picker>
             <Input placeholder='Nombre de empresa' name="company" onChange={e => this.handleCompanyChange(e)}  />
             <Input placeholder='Direccion de empresa u oficina' name="address" onChange={e => this.handleAddressChange(e)}  />
             <Picker style={styles.picker} onChange={e => this.handleSalaryChange(e)}>
             <Picker.Item label="--Seleccione--"/>
               <Picker.Item label="5,000 a 9,999" value="5,000 a 9,999"/>
               <Picker.Item label="10,000 a 19,999" value="10,000 a 19,999"/>
               <Picker.Item label="mas de 20,000" value="mas de 20,000"/>
             </Picker>
             {/*<Input placeholder='Sueldo comprobable mensual' name="salary" onChange={e => this.handleSalaryChange(e)} />*/}
             
              <View style={{ marginVertical: 20 }}></View>  <GradientButton
      style={{ marginVertical: 8 }}
      textStyle={{ fontSize: 20 }}
      gradientBegin="#0175FF"
      gradientEnd="#0016FF"
      gradientDirection="vertical"
      height={50}
      width={300}
      radius={25}
      impact
      impactStyle='Light'
      onPressAction={() => this.apply()}>
      Continuar
    </GradientButton></form>
                <View style={{ marginVertical: 20 }}></View>
<Text style={styles.blacktext}>Información legal</Text>

<Text style={styles.welcomeMsg}>Términos y condiciones</Text>

<Text style={styles.welcomeMsg}>Política y Aviso de Privacidad</Text>
<hr></hr>
<Text style={styles.textpolitica}>Tasa de interés mensual ordinaria aplicable desde 3.99% sin IVA. Por defecto, la tasa de interés anual fija es de 47.8% más IVA por un crédito de $70,000. Costo Anual Total (CAT) Promedio Ponderado: 106.75% sin IVA para fines informativos. Calculado a un plazo de 60 quincenas. Fecha de Cálculo: Febrero 28 2021.
Es el costo anual total del financiamiento expresado en términos de porcentaje anual, que para efectos informativos y comparativos, incluye todos los costos y gastos relacionados a tu préstamo. CAT máximo anual sin IVA: 61.7% por un crédito de $70,000 en 60 pagos quincenales. Tiempo mínimo de pago: 61 día después de emitida la línea de crédito. 
Tiempo máximo de pago: 900 días o 60 quincenas. TAE mínimo 60.65% - TAE máximo. Ejemplo: Crédito de 70.000 MXN a devolver en 30 meses. Interés fijo anual 47.88%. Cuota mensual 4.265.02 MXN. Sin comisión de apertura, estudio o por cancelación anticipada. TAE 74.74%. Cantidad total a devolver 127,950.60 MXN. El tipo de interés variará en función del importe.</Text>

{/*}
              <TouchableHighlight onPress={() => this.viewLoans()}>
                <View style={styles.loanHistory}>
                      <Text style={{color: '#1f618d'}}>View Loan History</Text>
                </View>
        </TouchableHighlight> */}
<br></br><Image source={require('../assets/logo-ssl-trust.png')} style={{flex:1,width:100, height:35}} />
            </View>
          </View>
        )
    }

}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: 0,
      backgroundColor: '#ffffff',
    },
    picker: {
      width:"90%",
      height:30,
      marginLeft:10,
      borderBottomWidth:1,
      borderWidth:0,
      borderBottomColor:"#6e6e6e",
    },
    welcome: {
      alignItems: 'center',
      justifyContent: 'center',
      padding:20,
    },
    header: {
      backgroundColor: '#0175FF'
    },
    innerHeader: {
      padding: 20,
    },
    welcomeMsg: {
      padding: 20,
      fontSize: 20,
      textAlign: 'center',
      color: '#0175FF'
    },
    amount:{
        padding:5,
        fontSize:30,
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#000'
    },
    blacktext:{
        color:'#000',
        fontSize:20,
        fontWeight:'bold'
    },
    textpolitica:{
        color:'#000',
        fontSize:10,
        fontWeight:'normal'
    },
    codigo:{
        color:'gray',
        fontSize:14,
        textAlign:'left',
    },
    loanHistory: {
      borderColor: '#0175FF',
      borderWidth: 1,
      borderRadius: 4,
      padding: 20,
      alignItems: 'center',
      justifyContent: 'center',
    }
  });