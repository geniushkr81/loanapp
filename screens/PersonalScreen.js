import React, {useState, useRef} from 'react';
import { Button, StyleSheet, Text, TextInput, View, TouchableOpacity,Image, TouchableHighlight } from 'react-native';
import { Ionicons as Icon } from '@expo/vector-icons';
import Slider from '@react-native-community/slider';
import { Input } from 'react-native-elements';
import GradientButton from 'react-native-gradient-buttons';
import AsyncStorage from '@react-native-community/async-storage';
import { MaskedTextInput } from 'react-native-mask-text';   
import DatePicker from 'react-datepicker';


export default class PersonalScreen extends React.Component {
  

  constructor(props){
  super(props);
  const getPhoneFunction = () => {
    AsyncStorage.getItem('Phone').then(value => 
      this.setState({phone:value})
    );
  }
   
  getPhoneFunction();
  // get email
  const getEmailFunction = () => {
    AsyncStorage.getItem('Email').then(value => 
      this.setState({email:value})
    );
  }
  getEmailFunction();
  // get amount
  const getAmountFunction = () => {
    AsyncStorage.getItem('Amount').then(value => 
      this.setState({amount:value})
    );
  }
  getAmountFunction();
  }
  // set name
  handleNameChange = (e) => {
    this.state.username = e.target.value;
   // alert(this.state.phone);
   //alert(e.target.value);
   AsyncStorage.setItem('Username', this.state.username);
  }
  // set first lastname
  handleFLastNameChange = (e) => {
    this.state.first_lastname = e.target.value;
   // alert(this.state.phone);
   AsyncStorage.setItem('FLastname', this.state.first_lastname);
  }
  // set second lastname
  handleSLastNameChange = (e) => {
    this.state.second_lastname = e.target.value;
   // alert(this.state.phone);
   AsyncStorage.setItem('SLastname', this.state.second_lastname);
  }
  // set birdthday
  handleBirdthdayChange = (e) => {
    console.log(e.target.value);
    this.state.birdthday = e.target.value;
   // alert(this.state.phone);
   AsyncStorage.setItem('Birdthday', this.state.birdthday);
  }
  // set curp
  handleCurpChange = (e) => {
    this.state.curp = e.target.value;
   // alert(this.state.phone);
   AsyncStorage.setItem('Curp', this.state.curp);
  }

    static defaultProps = {
        value:0,
    };
    state = {
      datevisible: true,
        mode:'date',
        show: false,
        startDate: new Date(),
        value: this.props.value,
        phone: this.props.phone,
        email: this.props.email,
        amount: this.props.amount,
        username: "",
        first_lastname:"",
        second_lastname:"",
        birdthday:"",
        curp:"",
    };

    // adding our home screen to the drawer navigation
    static navigationOptions = {
        drawerLabel: 'App prestamos',
    };

    // method to navigate to loan application screen
    apply(){
        const { navigate } = this.props.navigation
        navigate('Contact')
    }
    onChange = (event, selectedData) => {

    }
    // format currency
    currencyFormat(num) {
        return '$' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
     }
    // method to calculate amount and payments
    calculate(value){
        this.setState({value: value})
    }
  
    handleSubmit = (e) => {
var nameValid = false;
var lastnameFValid = false;
var lastnameSValid = false;
var birdthdayValid = false;
var curpValid = false;

      if(this.state.username === ""){
        alert("El campo nombre es requerido!!!");
        nameValid = false;
      }else {
        nameValid = true;
      }
      if(this.state.first_lastname === ""){
        alert("El campo apellido paterno es requerido!!!");
        lastnameFValid = false;
      }else {
        lastnameFValid = true;
      }
      if(this.state.second_lastname === ""){
        alert("El campo apellido materno es requerido!!!");
        lastnameSValid = false;
      }else{
        lastnameSValid = true;
      }
      if(this.state.birdthday === ""){
        alert("El campo nacimiento es requerido!!!");
        birdthdayValid = false;
      }else{
        birdthdayValid = true;
      }
      if(this.state.curp === ""){
        alert("El campo CURP es requerido!!!");
        curpValid = false;
      }else{
        curpValid = true;
      }

      const client = {
        name:'',
        email: this.state.email,
        phone: this.state.phone,
        amount: this.state.amount,
      };
      if(nameValid && lastnameFValid && lastnameSValid && birdthdayValid && curpValid){
        const { navigate } = this.props.navigation
        navigate('Contact')
      }else{
        console.log("name:"+nameValid + "first:" + lastnameFValid + "second:" + lastnameSValid + "birdthday:" + birdthdayValid + "curp:" + curpValid);
      }
    }
    
    hideDatePicker = () =>{

    };
    showDate = () => {
      console.log("clicked....")
      this.state.show = true;
    };
    changeDate = (date) => {
      console.log(date);
      this.setState({startDate:date})
      this.state.birdthday = date;
      AsyncStorage.setItem('Birdthday', this.state.birdthday);
    }

    handleConfirm = (date) => {
      console.log("A date has been picked: ", date);
    };
  
    // method to navigate to loan history screen

    render() {
    
    
        return (
          
            <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity style={styles.innerHeader} onPress={() => {this.props.navigation.toggleDrawer();}}>
                </TouchableOpacity>
            </View>
            <View style={styles.welcome}>
              <View style={{marginVertical: 10}}></View>
             {/* <Text>Value:{this.state.phone} : {this.state.email} : {this.state.amount}</Text>*/}
              <Text style={styles.welcomeMsg}>Compartenos tu información personal.</Text>
              <Text style={styles.amount}>Estas solicitando: ${this.state.amount} mxn</Text>
             <form>
             <Input placeholder='Nombre(s)' name="username" onChange={e => this.handleNameChange(e)} />
             <Input placeholder='Apellido paterno' name="first_lastname" onChange={e => this.handleFLastNameChange(e)} />
             <Input placeholder='Apellido materno' name="second_lastname" onChange={e => this.handleSLastNameChange(e)} />
        {/*
<MaskedTextInput mask="99-99-9999" placeholder='dd-mm-aaaa' onChangeText={(text, rawText) => {
  console.log(text);
  console.log(rawText);
}}
name="birdthday"
onChange={e => this.handleBirdthdayChange(e)}
style={styles.inputdate}/> */}
<div>
  <style>
    
    {`.date-picker input {
      width:90%;
      height: 30px;
      border-width:0px;
      font-size:18px;
      padding-left:20px;
      border-bottom: 1px solid gray;
    }`
    }
  </style>

<DatePicker wrapperClassName='date-picker' selected={this.state.startDate} onSelect={(date) => this.changeDate(date)} scrollableMonthYearDropdown showMonthDropdown showYearDropdown peekNextMonth dropdownMode='select' />
</div>
             <Input placeholder='CURP' name="curp" onChange={e => this.handleCurpChange(e)} />
             
              <View style={{ marginVertical: 20 }}></View> 
              <GradientButton
      style={{ marginVertical: 8 }}
      textStyle={{ fontSize: 20 }}
      gradientBegin="#0175FF"
      gradientEnd="#0016FF"
      gradientDirection="vertical"
      height={50}
      width={300}
      radius={25}
      impact
      impactStyle='Light'
      onPressAction={this.handleSubmit.bind(this)}>
      Continuar
    </GradientButton>
    </form>
                <View style={{ marginVertical: 20 }}></View>
<Text style={styles.blacktext}>Información legal</Text>

<Text style={styles.welcomeMsg}>Términos y condiciones</Text>

<Text style={styles.welcomeMsg}>Política y Aviso de Privacidad</Text>
<hr></hr>
<Text style={styles.textpolitica}>Tasa de interés mensual ordinaria aplicable desde 3.99% sin IVA. Por defecto, la tasa de interés anual fija es de 47.8% más IVA por un crédito de $70,000. Costo Anual Total (CAT) Promedio Ponderado: 106.75% sin IVA para fines informativos. Calculado a un plazo de 60 quincenas. Fecha de Cálculo: Febrero 28 2021.
Es el costo anual total del financiamiento expresado en términos de porcentaje anual, que para efectos informativos y comparativos, incluye todos los costos y gastos relacionados a tu préstamo. CAT máximo anual sin IVA: 61.7% por un crédito de $70,000 en 60 pagos quincenales. Tiempo mínimo de pago: 61 día después de emitida la línea de crédito. 
Tiempo máximo de pago: 900 días o 60 quincenas. TAE mínimo 60.65% - TAE máximo. Ejemplo: Crédito de 70.000 MXN a devolver en 30 meses. Interés fijo anual 47.88%. Cuota mensual 4.265.02 MXN. Sin comisión de apertura, estudio o por cancelación anticipada. TAE 74.74%. Cantidad total a devolver 127,950.60 MXN. El tipo de interés variará en función del importe.</Text>

{/*}
              <TouchableHighlight onPress={() => this.viewLoans()}>
                <View style={styles.loanHistory}>
                      <Text style={{color: '#1f618d'}}>View Loan History</Text>
                </View>
        </TouchableHighlight> */}
<br></br><Image source={require('../assets/logo-ssl-trust.png')} style={{flex:1,width:100, height:35}} />
            </View>
          </View>
        )
    }

}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: 0,
      backgroundColor: '#ffffff',
    },
    welcome: {
      alignItems: 'center',
      justifyContent: 'center',
      padding:20,
    },
    header: {
      backgroundColor: '#0175FF'
    },
    innerHeader: {
      padding: 20,
    },
    inputdate: {
      height:35,
      width:200,
      fontSize:18,
      margin:10,
      borderBottomColor:"#b6b6b6",
      borderBottomWidth:1,
    },
    welcomeMsg: {
      padding: 20,
      fontSize: 20,
      textAlign: 'center',
      color: '#0175FF'
    },
    amount:{
        padding:5,
        fontSize:30,
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#000'
    },
    blacktext:{
        color:'#000',
        fontSize:20,
        fontWeight:'bold'
    },
    textpolitica:{
        color:'#000',
        fontSize:10,
        fontWeight:'normal'
    },
    codigo:{
        color:'gray',
        fontSize:14,
        textAlign:'left',
    },
    loanHistory: {
      borderColor: '#0175FF',
      borderWidth: 1,
      borderRadius: 4,
      padding: 20,
      alignItems: 'center',
      justifyContent: 'center',
    }
  });