import * as Font from 'expo-font';

export default useFonts = async () => 
    await Font.loadAsync({
        Monserrat: require('../assets/fonts/Montserrat-Regular.ttf')
    });
