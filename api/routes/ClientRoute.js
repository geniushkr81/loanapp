'use strict';

module.exports = function (app){
    var client = require('../controllers/ClientController');

    // clients routes
    app.route('/clients')
        .post(client.createClient);

    
}