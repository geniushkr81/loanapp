'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ClientSchema = new Schema({
    amount: {
        type: String,
    },
    name: {
        type: String,
    },
    email: {
        type: String,
    },
    phone: {
        type: String,
    },
    username: { type: String},
    first_lastname: { type: String},
    second_lastname: { type: String},
    birdthday: { type: String},
    curp: { type: String},
    street:{ type: String},
    colony:{ type: String},
    cp:{ type: String},
    country:{ type: String},
    city:{ type: String},
    government:{ type: String},
    nameref01:{ type: String},
    nameref02:{ type: String},
    relref01:{ type: String},
    relref02:{ type: String},
    phoneref01:{ type: String},
    phoneref02:{ type: String},
    occupation: { type: String},
    company: { type: String},
    address: { type: String},
    salary: { type: String},
    holder:{ type: String},
    clabe:{ type: String},
    bank:{ type: String},
    Created_date: {
        type: Date,
        default: Date.now
    },
});
module.exports = mongoose.model('Clients', ClientSchema);