var express = require("express"),
    app = express(),
    http = require("http"),
    https = require('https'),
    fs = require('fs'),
    server = http.createServer(app),
    bodyParser = require("body-parser"),
    methodOverride = require("method-override"),
    Client = require('./models/ClientModel'),
    mongoose = require("mongoose");
    axios = require('axios').default;
var cors = require('cors');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(cors());

var routes = require('./routes/ClientRoute');
// importing route
routes(app);
app.set('view engine', 'ejs');
var clients = [];
app.get('/clients', (req, res) => {
    res.render('/home/loanapp/api/views/pages/tablegrid',{clients});
    console.log(clients);
})


app.get('/cp', async (req,res) => {
    const value_cp = req.query.value;
    try{
        const response = await axios.get('https://apisgratis.com/api/cp/v2/colonias/cp', {
            params: {
              valor: value_cp
            }
          });
          res.status(200).json(response.data);
          res.send(response.data);
    } catch (err) {
        res.status(500).json({message:err});
    }
});

mongoose.connect("mongodb://localhost:27017/loans", function(err, db){
    if(err) throw err;
    console.log("Databse created!");
    db.collection('clients').find().toArray(function(err,result){
        if (err) {
            throw err;
        }
        clients = result;
        console.log(result);
    });
});
https.createServer({
	cert:fs.readFileSync('ssl.cert'),
	key: fs.readFileSync('ssl.key')
   }, app).listen(3100, function() {
        console.log("Node server running on https://prestaclick.com.mx:3100");
   });
